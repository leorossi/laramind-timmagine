@extends('layouts.main')

@section('content')
<h1>
    {{ $post->title }} (di {{ $post->user->name }} che ha scritto {{ $post->user->posts()->count() }} post)
</h1>

<p> {{ $post->body }}</p>


    <div id="comments">
        <h2>Commenti</h2>
        @foreach($post->comments as $comment)
            <div>
                <h3>Di {{ $comment->user->name }} scritto alle {{ $comment->created_at }}</h3>
                {{ $comment->body }}
                <hr>
            </div>
        @endforeach
    </div>
@endsection