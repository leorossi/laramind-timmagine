@extends('layouts.main')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <h1>Crea nuovo post</h1>

    <form action="{{ route('posts.store') }}" method="POST" class="form">
        @csrf
        <div class="form-group">
            <label for="title">Titolo:</label>
            <input type="text" class="form-control" name="title">

        </div>

        <div class="form-group">
            <label for="body">Testo:</label>
            <textarea name="body" class="form-control"></textarea>

        </div>

        <div class="form-group">
            <button class="btn btn-primary">Salva</button>
        </div>
    </form>
@endsection