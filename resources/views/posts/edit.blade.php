@extends('layouts.main')

@section('content')
    <h1>Modifica post {{ $post->id }}</h1>

    <form action="{{ route('posts.update', $post) }}" method="POST" class="form">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">Titolo:</label>
            <input type="text" class="form-control" name="title" value="{{ $post->title }}">

        </div>

        <div class="form-group">
            <label for="body">Testo:</label>
            <textarea name="body" class="form-control">{{ $post->body }}</textarea>

        </div>

        <div class="form-group">
            <button class="btn btn-primary">Salva</button>
        </div>
    </form>
@endsection