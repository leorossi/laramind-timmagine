@extends('layouts.main')

@section('custom_head')

@endsection


@section('content')

<h1>Lista post</h1>
<div>
    {{ $articoli->links() }}
</div>
<ul>
    @foreach($articoli as $post)
        <li>
            [ {{ $post->state }} ]
            <a href="{{ route('posts.show', $post) }}">
                {{ $post->title }}
            </a>
            [ #{{ $post->id }} ]
            <a href="{{ route('posts.edit', $post )}}" class="btn btn-link btn-primary btn-sm">Modifica</a>
            <div class="btn-group">

                <form action="{{ route('posts.destroy', $post) }}" method="POST">
                    @csrf
                    @method('delete')
                    <button class="btn btn-danger">
                        Elimina
                    </button>
                </form>

            </div>
            <div>
                @foreach($post->tags as $tag)
                    <span class="badge badge-secondary">{{ $tag->name }}</span>
                @endforeach
            </div>
        </li>
    @endforeach
</ul>
    <div>
        {{ $articoli->links() }}
    </div>
    <div>
        <a href="{{ route('posts.create')}}" class="btn btn-link btn-primary">Nuovo</a>
    </div>
@endsection