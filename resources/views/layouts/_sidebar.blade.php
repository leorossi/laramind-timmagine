<h2>Ultimi post</h2>

<ul>
    @foreach($latest_posts as $post)
    <li>
        <a href="{{ route('posts.show', $post) }}">
            {{ $post->title }}
        </a>
    </li>
    @endforeach
</ul>
