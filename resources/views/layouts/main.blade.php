<html>
<head>
    <title>{{ config('app.name') }}</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    @yield('custom_head')
</head>
<body>
<div class="container">
    @include('layouts._navbar')
    <div class="row">
        <div class="col-8">
            @yield('content')
        </div>
        <div class="col-4">
            @include('layouts._sidebar')
        </div>
    </div>


</div>

</body>
</html>

