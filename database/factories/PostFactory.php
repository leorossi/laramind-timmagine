<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->catchPhrase(),
        'body' => $faker->text(rand(100, 1000)),
        'state' => $faker->randomElement(['draft', 'published']),
        'user_id' => \App\User::all()->random(1)->first()->id
    ];
});
