<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Comment::class, function (Faker $faker) {
    $user = \App\User::inRandomOrder()->first()->id;
    return [
        'body' => $faker->text(rand(100, 1000)),
        'user_id' => $user
    ];
});
