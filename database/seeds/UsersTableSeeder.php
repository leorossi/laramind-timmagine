<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $oldAdmin = \App\User::where('email', 'admin@domain.com')->first();
        if (!$oldAdmin) {
            \App\User::create([
                'email' => 'admin@domain.com',
                'name' => 'admin',
                'password' => \Illuminate\Support\Facades\Hash::make('password'),
            ]);
        }
    }
}
