<?php

namespace App\Providers;

use App\Http\Views\Composers\LatestPostsViewComposer;
use App\Observers\UserObserver;
use App\Post;
use App\Services\LoremIpsumGenerator;
use App\User;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(LatestPostsViewComposer::class);
        View::composer(['layouts._sidebar', 'layouts._navbar'], LatestPostsViewComposer::class);
        
        $this->app->singleton('text_generator', function($app) {
            $apiKey = config('app.lorem_ipsum_api_key');
            return new LoremIpsumGenerator($apiKey);
        });
        User::observe(UserObserver::class);
    }
}
