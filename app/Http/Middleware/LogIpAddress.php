<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class LogIpAddress
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $additionalText = '')
    {
        Log::info('IP Address: ' . $request->ip() . ' [' . $request->method() . ' ' . $request->path() . '] ' . $additionalText);
        return $next($request);
    }
}
