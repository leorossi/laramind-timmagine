<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class QueryLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        DB::enableQueryLog();
        return $next($request);
    }
    
    public function terminate($request, $response)
    {
        file_put_contents('/tmp/test.txt', json_encode(DB::getQueryLog()));
    }
}
