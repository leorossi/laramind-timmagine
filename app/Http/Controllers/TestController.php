<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use App\Tag;
use App\User;
use Pippo;
class TestController extends Controller
{
    public function generateUsers() {
        $users = factory(User::class, 10)->create();
        dd($users);
    }
    
    public function generateComments() {
        $posts = Post::all();
        foreach ($posts as $post) {
            factory(Comment::class, 5)->create([
                'post_id' => $post->id,
            ]);
        }
        
    }
    
    public function generateTags() {
        $post = Post::find(7);
        
        $post->tags()->sync([1, 2, 5]);
        dd('done');
        
        $posts = Post::all();
        $tags = Tag::all();
        foreach ($posts as $post) {
            $randomTags = $tags->random(rand(1, 5));
            $post->tags()->sync($randomTags);
        }
        dd('done');
    }
    
    public function lorem() {
        return Pippo::generate();
//        $service = app()->make('TextGenerator');
//        return $service->generate();
    }
}
