<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    public function __construct() {
        $this->middleware('log.ip:ADDITIONAL_TEXT');
        $this->middleware('login.as:1')->only('index');
    }
    public function store(CreatePostRequest $request) {
        dd('validation passed');
        $user = Auth::user();
        $post = Post::make($request->all());
        $post->user()->associate($user);
        
        $post->save();
        return redirect()->route('posts.show', $post);
    }
    
    public function index() {
        $user = Auth::user();
        $request = request();
        if ($request->has('search')) {
            $posts = Post::where('title', 'LIKE', '%' . $request->query('search') . '%')
                ->orWhere('body', 'LIKE', '%' . $request->query('search') . '%')
                ->orderBy('title');
                
        } else {
            $posts = Post::with('tags');
        }
        $posts = $posts->filtered($user)->paginate();
        $posts->appends(['search' => $request->input('search')]);
        return view('posts.index', [
            'articoli' => $posts
        ]);
    }
    
    public function show(Post $post) {
        return view('posts.show', [
            'post' => $post
        ]);
    }
    
    public function create() {
        return view('posts.create');
    }
    
    public function edit(Post $post) {
        return view('posts.edit', [
            'post' => $post
        ]);
    }
    
    public function update(Request $request, Post $post) {
        $post->update($request->all());
        return redirect()->route('posts.show', $post);
    }
    
    public function destroy(Post $post) {
        $post->delete();
        return redirect()->route('posts.index');
    }
    
}
