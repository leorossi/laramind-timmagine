<?php

namespace App\Http\Views\Composers;

use App\Post;
use Illuminate\View\View;

class LatestPostsViewComposer {
    
    public function __construct() {
        $amount = 10;
        $this->latestPosts = Post::orderBy('created_at', 'DESC')->limit($amount)->get();
    }
    public function compose(View $view) {
        
        if ($view->name() == 'layouts._sidebar') {
            $amount = 5;
        }
    
        if ($view->name() == 'layouts._navbar') {
            $amount = 3;
        }
        return $view->with('latest_posts', $this->latestPosts->slice(0, $amount));
    }
}