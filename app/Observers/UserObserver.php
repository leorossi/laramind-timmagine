<?php

namespace App\Observers;

use App\User;
use Illuminate\Support\Str;

class UserObserver
{
    public function creating(User $user) {
        $user->api_token = Str::random(60);
    }
    
    public function deleted(User $user) {
    
    }
   
    public function updated(User $user) {
        dd($user->getOriginal('name'), $user->name);
    }
}
