<?php

namespace App;

use App\Scopes\EnabledScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{
    protected $fillable = [ 'title', 'body' ];
    
    public function comments() {
        return $this->hasMany(Comment::class);
    }
    
    public function user() {
        return $this->belongsTo(User::class);
    }
    
    public function tags() {
        return $this->belongsToMany(Tag::class, 'posts_tags')->withTimestamps();
    }
    
    public function scopeDraft(Builder $query) {
        return $query->where('state', 'draft');
    }
    
    public function scopePublished(Builder $query) {
        return $query->where('state', 'published');
    }
    
    public static function boot()
    {
        parent::boot();
    }
    
    public function scopeFiltered(Builder $query, $user) {
        if (!$user || $user->name != 'admin') {
            return $query->published();
        }
        return $query;
    }
}
