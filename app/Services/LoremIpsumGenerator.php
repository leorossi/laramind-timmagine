<?php

namespace App\Services;

use mysql_xdevapi\Exception;

class LoremIpsumGenerator {
    public function __construct($apiKey)
    {
        if ($apiKey == '') {
            throw new Exception();
        }
        $this->apiKey = $apiKey;
    }
    
    public function generate() {
        return 'APIKEY: ' . $this->apiKey . ' --> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac consectetur urna. Donec sodales eros sit amet est sodales porta. Pellentesque imperdiet sapien nisl, vel iaculis arcu porttitor pretium. Suspendisse sed eros aliquet, eleifend leo id, blandit nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus condimentum gravida nibh, sed rhoncus nisl semper eu. Vivamus pretium scelerisque elementum. Quisque aliquet accumsan malesuada. Mauris dignissim mollis diam, at tristique dui lobortis at.';
    }
}