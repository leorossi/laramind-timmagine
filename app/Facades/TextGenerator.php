<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class TextGenerator extends Facade {
    protected static function getFacadeAccessor()
    {
        return 'text_generator';
    }
    
}