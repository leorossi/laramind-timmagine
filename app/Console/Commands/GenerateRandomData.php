<?php

namespace App\Console\Commands;

use App\Comment;
use App\Post;
use App\User;
use Illuminate\Console\Command;
use Psy\Util\Str;

class GenerateRandomData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:generate {--users=10} {--posts=100} {--comments=5}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates random posts and comments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Generate users
        $users = factory(User::class, intval($this->option('users')))->create();
        // Generate posts (with tags)
        $posts = factory(Post::class, intval($this->option('posts')))->create();
        // Generate comments
        foreach ($posts as $post) {
            factory(Comment::class, intval($this->option('comments')))->create([
                'post_id' => $post->id
            ]);
        }
        
        $this->info('Data Generated');
    }
}
