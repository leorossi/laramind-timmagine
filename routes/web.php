<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/', function () {
    return view('welcome');
});

//Route::post('/posts', 'PostsController@store')->name('posts.store');
//Route::get('/posts', 'PostsController@index')->name('posts.index');
//Route::get('/posts/{post}', 'PostsController@show')->name('posts.show')->where('post', '[0-9]+');
//Route::get('/posts/create', 'PostsController@create')->name('posts.create');
//Route::put('/posts/{post}', 'PostsController@update')->name('posts.update');
//Route::get('/posts/{post}/edit', 'PostsController@edit')->name('posts.edit');
//Route::delete('/posts/{post}', 'PostsController@destroy')->name('posts.destroy');


Route::resource('posts', 'PostsController');

Route::resource('comments', 'CommentsController');

Auth::routes();

Route::middleware('log.ip')->get('/home', 'HomeController@index')->name('home');

Route::get('/lorem', 'TestController@lorem');





//class Petrol implements Fuel {
//    public function getPricePerLiter()
//    {
//        return 1.6;
//    }
//}
//
//
//class Diesel implements Fuel {
//    public function getPricePerLiter()
//    {
//        return 1.5;
//    }
//}
//
//interface Fuel {
//    public function getPricePerLiter();
//}
//
//class Car {
//    public function __construct(Fuel $fuel) {
//        $this->litersPer100Km = 10;
//        $this->fuel = $fuel;
//    }
//
//    public function pricePer100Km() {
//        return $this->litersPer100Km * $this->fuel->getPricePerLiter();
//    }
//}
//
//$app = app();
//$app->singleton(Fuel::class, Petrol::class);
//$app->singleton(Car::class);
////$fuel = new Fuel();
////$car = new Car($fuel);
//
//$car = $app->make(Car::class);
//dump($car->pricePer100Km(), $car);
//
//
//$car2 = $app->make(Car::class);
//dump($car2->pricePer100Km(), $car2);
//
//dd('done');
//






